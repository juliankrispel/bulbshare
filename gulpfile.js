var less = require('gulp-less');
var gulp = require('gulp');
var connect = require('gulp-connect');

gulp.task('less', () => {
  gulp.src('./less/main.less')
  .pipe(less())
  .pipe(gulp.dest('./public', { overwrite: true }));
});

gulp.task('watch', () => {
  return gulp.watch('less/**/*.less', ['less']);
});

gulp.task('server', () => {
  connect.server({
    root: 'public',
    livereload: true
  });
});

gulp.task('build', ['less']);

gulp.task('default', ['watch', 'server']);
