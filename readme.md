#To run in development:

- Clone the repository
- Run the shell command `yarn` if you use [yarn](https://yarnpkg.com/) or `npm install` if you use the npm installer.
- Run `gulp` which will spin up the default build task, spinning up a web server and compiling less files.
